﻿/*
 * Thanks to Carlos Delgado for this post
 * https://ourcodeworld.com/articles/read/128/how-to-play-pause-music-or-go-to-next-and-previous-track-from-windows-using-c-valid-for-all-windows-music-players
 */

global using static Media_Controls.Logger;
using System;
using System.Runtime.InteropServices;
using ModLoader.Framework;
using ModLoader.Framework.Attributes;
using VTOLAPI;


namespace Media_Controls;

[ItemId("marsh.mediacontrols")]
public class MediaControls : VtolMod
{
    public const int KEYEVENTF_EXTENTEDKEY = 1;
    public const int KEYEVENTF_KEYUP = 0;
    public const int VK_MEDIA_NEXT_TRACK = 0xB0;// code to jump to next track
    public const int VK_MEDIA_PLAY_PAUSE = 0xB3;// code to play or pause a song
    public const int VK_MEDIA_PREV_TRACK = 0xB1;// code to jump to prev track
    
    private static MediaControls _instance;
    private MusicVolumeManager _musicVolume;

    [DllImport("user32.dll")]
    public static extern void keybd_event(byte virtualKey, byte scanCode, uint flags, IntPtr extraInfo);

    private void Awake()
    {
        _instance = this;
        _musicVolume = new MusicVolumeManager();
        VTAPI.SceneLoaded += SceneLoaded;
        VTAPI.MissionReloaded += MissionReloaded;
    }

    public static void PressButton(MediaKey key)
    {
        _instance.Press(key);
    }

    private void Press(MediaKey key)
    {
        switch (key)
        {
            case MediaKey.PlayPause:
                Log("Play/Pause Pressed");
                keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                break;
            case MediaKey.NextTrack:
                Log("Next Track Pressed");
                keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                break;
            case MediaKey.PreviousTrack:
                Log("Previous Track Pressed");
                keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENTEDKEY, IntPtr.Zero);
                break;
        }
    }

    private void Update()
    {
        if (_musicVolume.IsSceneWithRadioControls(VTAPI.currentScene))
        {
            _musicVolume.SynchronizeMusicVolume();
        }
    }

    private void SceneLoaded(VTScenes scene)
    {
        _musicVolume.SceneLoaded(scene);
    }

    private void MissionReloaded()
    {
        _musicVolume.SceneLoaded(VTAPI.currentScene);
    }

    private void OnApplicationQuit()
    {
        _musicVolume.ResetSystemVolumeToOriginal();
    }

    public override void UnLoad()
    {
        OnApplicationQuit();
        Log("Bye Bye");
    }
}