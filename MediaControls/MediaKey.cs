namespace Media_Controls;

public enum MediaKey
{
    PlayPause,
    NextTrack,
    PreviousTrack
}