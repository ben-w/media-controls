global using static Media_Controls.Logger;
using System.Collections.Generic;
using System.Threading;
using CSCore.CoreAudioAPI;
using VTOLAPI;

namespace Media_Controls;

class MusicVolumeManager
{
    public static MusicVolumeManager instance;

    /*
     * Prioritized list of exe names to detect as music programs with the included detection logic. You can determine the name of new programs
     * by running them and then finding the exe name in the "Details" tab of the "Task Manager". One subtle point to be aware of is that
     * the program has to be considered a "Multimedia" program
     */
    private readonly string[] _knownMusicPrograms = ["Spotify", "vlc", "wmplayer", "chrome", "msedge", "firefox", "brave", "TIDALPLAYER", "iTunes", "AppleMusic"];

    /* Cached value to control the system's application volume level and reset it */
    private VolumeControl _musicControl;

    /* UI Configurable Settings */
    private int _defaultRadioVolumePct = 25;
    private string _customMusicProgram = "";

    public MusicVolumeManager()
    {
        instance = this;
    }

    /* Settings no longer exist in the mod loader, but saving this for when they do
    public void AddSettings(VTMPScenarioSettings.Settings settings)
    {
        settings.CreateCustomLabel("Default Music Volume (0-100)");
        settings.CreateIntSetting("Default = 25", SetDefaultRadioVolumePctSetting, _defaultRadioVolumePct, 0, 100);

        settings.CreateCustomLabel("Custom Music Program (exe) Name");
        settings.CreateStringSetting("Default = Null (automatic detection)", SetCustomMusicProgram, _customMusicProgram);
    }
    

    void SetDefaultRadioVolumePctSetting(int volume)
    {
        Log($"Setting default radio volume pct to: {volume}");
        _defaultRadioVolumePct = volume;
    }

    void SetCustomMusicProgram(string program)
    {
        Log($"Setting custom music program to: {program}");
        _customMusicProgram = program;
    }
    */

    public bool IsSceneWithRadioControls(VTScenes scene)
    {
        return scene switch
        {
            VTScenes.Akutan or VTScenes.CustomMapBase => true,
            _ => false
        };
    }

    public void SceneLoaded(VTScenes scene)
    {
        if (!IsSceneWithRadioControls(scene)) 
            return;
        Log($"SceneLoaded({scene}): Detecting music program and setting radio to default volume");

        // Query for the music control every time a scene loads so that you can change music programs or settings between
        // scenes and not have to reload the game to get a new handle.
        BackgroundDetectAndCacheMusicVolumeControl();
    }

    // Resets the system application volume to the original value it was when the game was started. Used to clean up
    // any in-game modifications to system level configuration.
    //
    // For example, if you use Spotify and you reduce the volume in-game with this mod, when it exits Spotify will 
    // forever be in a quiet state until you manually go into the system mixer and bump it back up. 
    // This method should be used so that we reset it for them when the game exits.
    public void ResetSystemVolumeToOriginal()
    {
        if (_musicControl == null) 
            return;

        Log($"Resetting mixer volume of {_musicControl.ProcessName} to: {_musicControl.OriginalMasterVolume}");
        _musicControl.MasterVolume = _musicControl.OriginalMasterVolume;
    }

    public void SynchronizeMusicVolume()
    {
        if (CockpitRadio.instance == null || _musicControl == null)
            return;

        var radioSrc = CockpitRadio.instance.audioSource;
        if (_musicControl.MasterVolume != radioSrc.volume)
        {
            Log($"Updating music volume of {_musicControl.ProcessName} to: {radioSrc.volume}");
            _musicControl.MasterVolume = radioSrc.volume;
        }
    }

    public float DefaultRadioVolume()
    {
        return _defaultRadioVolumePct / 100f;
    }

    private void BackgroundDetectAndCacheMusicVolumeControl()
    {
        // Audio APIs are required to be accessed through an MTA thread. We cache
        // the result rather than having to keep looking it up.
        var t = new Thread(DetectAndCacheMusicVolumeControl);
        t.SetApartmentState(ApartmentState.MTA);
        t.Start();
        t.Join();
    }

    private void DetectAndCacheMusicVolumeControl()
    {
        Log("Detecting music volume controls...");
        // TODO: Should we be properly disposing of these IDisposable objects?
        var enumerator = new MMDeviceEnumerator();
        var device = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
        var sessionManager = AudioSessionManager2.FromMMDevice(device);
        var sessionEnumerator = sessionManager.GetSessionEnumerator();

        // First we build up a list of structured objects by querying the session enumerator for possible audio sessions.
        var potentialControls = new List<VolumeControl>();
        foreach (var rawSession in sessionEnumerator)
        {
            var audioSession = rawSession.QueryInterface<AudioSessionControl2>();
            var simpleVolume = rawSession.QueryInterface<SimpleAudioVolume>();
            if (audioSession.Process == null) continue;
            Log($"Detected Name: {audioSession.Process.ProcessName} | PID: {audioSession.ProcessID} | Master Volume: {simpleVolume.MasterVolume}");
            potentialControls.Add(new VolumeControl(audioSession, simpleVolume));
        }

        // Search for our preferred music program and use that if detected.
        if (_customMusicProgram.Length > 0)
        {
            foreach (var control in potentialControls)
            {
                if (control.ProcessName.Contains(_customMusicProgram))
                {
                    CacheVolumeControl(control);
                    return;
                }
            }
        }

        // Search for automatically detected programs in order of priority
        foreach (var name in _knownMusicPrograms)
        {
            foreach (var control in potentialControls)
            {
                if (control.ProcessName.Contains(name))
                {
                    CacheVolumeControl(control);
                    return;
                }
            }
        }

        LogError("Unable to detect a music program.");
    }

    private void CacheVolumeControl(VolumeControl nextControl)
    {
        Log($"Selecting: {nextControl.ProcessName} ({nextControl.ProcessID}) | Starting volume: {nextControl.MasterVolume}");

        // This logic ensures that we properly clean up if the detected music program changes between evaluations of this method. This can happen
        // because the mod settings change or the open programs on the OS change. It must be called before we change the _musicControl variable.
        if (_musicControl != null && _musicControl.ProcessName != nextControl.ProcessName)
        {
            Log($"Process changed from {_musicControl.ProcessName} to {nextControl.ProcessName}, resetting original session volume");
            ResetSystemVolumeToOriginal();
        }

        _musicControl = nextControl;
    }
}