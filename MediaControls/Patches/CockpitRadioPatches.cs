﻿using HarmonyLib;

namespace Media_Controls.Patches;

[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PlayButton))]
public static class CockpitRadioPlayButton
{
    [HarmonyPrefix]
    public static bool Prefix()
    {
        MediaControls.PressButton(MediaKey.PlayPause);
        return false;
    }
}

[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.PrevSong))]
public static class CockpitRadioPrevSong
{
    [HarmonyPrefix]
    public static bool Prefix()
    {
        MediaControls.PressButton(MediaKey.PreviousTrack);
        return false;
    }
}

[HarmonyPatch(typeof(CockpitRadio), nameof(CockpitRadio.NextSong))]
public static class CockpitRadioNextSong
{
    
    [HarmonyPrefix]
    public static bool Prefix()
    {
        MediaControls.PressButton(MediaKey.NextTrack);
        return false;
    }
}