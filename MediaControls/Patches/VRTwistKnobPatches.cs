﻿using HarmonyLib;

namespace Media_Controls.Patches;

[HarmonyPatch(typeof(VRTwistKnob), nameof(VRTwistKnob.OnLoadVehicleData))]
public static class VRTwistKnobPatches
{
    public static bool Prefix(VRTwistKnob __instance)
    {
        // For the RadioVolume knob, we want to always set it to our default volume when a vehicle is loaded. By default
        // it will use the last saved value, so we set manually set it to ours and then return false.
        if (__instance.gameObject.name == "RadioVolumeKnob")
        {
            var value = MusicVolumeManager.instance.DefaultRadioVolume();
            __instance.startValue = value;
            __instance.SetKnobValue(value);
            return false;
        }

        return true;
    }
}