using CSCore.CoreAudioAPI;

namespace Media_Controls;

class VolumeControl
{
    public float MasterVolume
    {
        get => _volume.MasterVolume;
        set => _volume.MasterVolume = value;
    }
    
    public readonly float OriginalMasterVolume;
    public readonly string ProcessName;
    public readonly int ProcessID;

    private readonly SimpleAudioVolume _volume;

    public VolumeControl(AudioSessionControl2 session, SimpleAudioVolume volume)
    {
        _volume = volume;
        ProcessName = session.Process.ProcessName;
        ProcessID = session.ProcessID;
        OriginalMasterVolume = volume.MasterVolume;
    }
}