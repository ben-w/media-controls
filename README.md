![Wide Thumbnail](https://gitlab.com/ben-w/media-controls/-/raw/master/Wide%20Thumbnail.png)

This is the Media Controls mod source code for VTOL VR. You can view the mod [on steam](https://steamcommunity.com/sharedfiles/filedetails/?id=3311527930).

## Dependencies

- 0Harmony.dll
- Assembly-CSharp.dll
- ModLoader.Framework.dll
- UnityEngine.AudioModule.dll
- UnityEngine.CoreModule.dll
- UnityEngine.dll
- VTOLAPI.dll

VTOL API can be found in your workshop folder `steamapps\workshop\content\3018410\3265689427`

The rest can be found in your VTOL VR Folder `steamapps\common\VTOL VR\VTOLVR_Data\Managed`

## Contributions

@jakecraige for adding support for the volume knob to control just the music player in !1 update 2.0.0

@thegreatoverlordofallcheese for adding chrome and edge into the list of supported multi media applications